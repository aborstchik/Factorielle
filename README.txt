==================================================================
                          FACTORIELLE
==================================================================

Principe du programme
__________________________________________________________________
Demande à un utilisateur d'entrer un entier naturel n, calcule puis affiche n!.
__________________________________________________________________


Fichiers
__________________________________________________________________
algo_td6-bonus.txt: algorithme

prog_td6-bonus.c: code source du programme.

VERSION.txt: version actuelle du projet

Makefile
__________________________________________________________________


Compilation et exécution (Linux)
__________________________________________________________________
Pour compiler, ouvrir un terminal, se déplacer dans le répertoire du projet et entrer:
>make

Le programme peut être exécuté après compilation avec:
>./prog_td6-bonus.out
__________________________________________________________________


Informations supplémentaires
__________________________________________________________________
Réalisé dans le cadre d'un module de programmation de la licence d'informatique de l'Université Paris XIII.
__________________________________________________________________

2021 - Creative Commons (CC0)
Alexis BORSTCHIK
